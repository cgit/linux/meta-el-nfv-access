OSTREE_BOOTLOADER ?= "grub"
EFI_PROVIDER_sota = "grub-efi"
PREFERRED_PROVIDER_virtual/bootloader_sota = "grub-efi"

WKS_FILE_sota = "efiimage-sota.wks"

OSTREE_INITRAMFS_FSTYPES ?= "cpio.gz"

# Set .otaimg to be used as source for generating hddimg
ROOTFS_sota = "${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.ota-ext4"

# OSTree initrd needs 'ramdisk_size' and 'rw' parameters in order to boot
OSTREE_KERNEL_ARGS ?= "rw"
