# efi_populate_common DEST BOOTLOADER
efi_populate_common() {
        # DEST must be the root of the image so that EFIDIR is not
        # nested under a top level directory.
        DEST=$1

        install -d ${DEST}${EFIDIR}

        ## ENEA_start ##
        # install -m 0644 ${DEPLOY_DIR_IMAGE}/$2-${EFI_BOOT_IMAGE} ${DEST}${EFIDIR}/${EFI_BOOT_IMAGE}
        ## ENEA_end ##

        EFIPATH=$(echo "${EFIDIR}" | sed 's/\//\\/g')
        printf 'fs0:%s\%s\n' "$EFIPATH" "${EFI_BOOT_IMAGE}" >${DEST}/startup.nsh

        ## ENEA_start ##
        # Install EFI binaries
        install -m 0644 ${DEPLOY_DIR_IMAGE}/${SBFOLDER}/*.efi ${DEST}${EFIDIR}

        # Install kernel and initramfs sig
        install -m 0644 ${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGETYPE}${SB_FILE_EXT} ${DEST}/${KERNEL_IMAGETYPE}${SB_FILE_EXT}
        install -m 0644 ${INITRD_LIVE}${SB_FILE_EXT} ${DEST}/initrd${SB_FILE_EXT}

        # Install UEFI keys
        install -d ${DEST}/uefi_sb_keys
        install -m 0644 ${DEPLOY_DIR_IMAGE}/user-keys/uefi_sb_keys/*.crt ${DEST}/uefi_sb_keys/
        install -m 0644 ${DEPLOY_DIR_IMAGE}/user-keys/uefi_sb_keys/*.esl ${DEST}/uefi_sb_keys/
        ## ENEA_end ##
}
