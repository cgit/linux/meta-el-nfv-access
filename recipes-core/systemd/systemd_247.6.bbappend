FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append_sota = " file://basic.conf.in"

GROUPADD_PARAM_${PN}_append_sota = "; -r render"

# systemd uses certain groups unless configured not to (e.g. journal logs are more
# broadly available to the 'wheel' group unless told otherwise), while some resources
# are using to the 'nobody' group. Configure systemd to:
# - not use the 'wheel' group (journal access will be restriced to root user);
# - use the proper group for 'nobody', which should have GID 65534 (for NFVA 'nogroup');
EXTRA_OEMESON += " \
    -Dwheel-group=false \
    -Dnobody-group=nogroup \
"

do_configure_prepend_sota() {
    cp ${WORKDIR}/basic.conf.in ${S}/sysusers.d/basic.conf.in
}

do_install_append () {
    # Update default udev rules for /dev/kvm to be less permissive
    sed -e 's/\(KERNEL=="kvm".*\)0666/\10660/' \
        -i ${D}${rootlibexecdir}/udev/rules.d/50-udev-default.rules
}
