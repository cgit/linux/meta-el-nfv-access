DESCRIPTION = "Image for the host side of the Enea Edge Runtime with ODM and NETCONF Edgelink customizations"

require images/enea-edge-host-common.inc

IMAGE_INSTALL += " \
    element-vcpe \
    "
