DESCRIPTION = "Image for building the SDK for the host side of the Enea Edge Runtime with ODM customizations"

require images/enea-edge-host-common.inc

IMAGE_INSTALL += " \
    element-odm-sdk \
    "

TOOLCHAIN_TARGET_TASK_append = " kernel-devsrc"

IMAGE_FEATURES += "dbg-pkgs dev-pkgs"
