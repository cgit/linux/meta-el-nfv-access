IMAGE_FSTYPES += "hddimg"
IMAGE_FSTYPES_remove = "wic"

REQUIRE_FILES = " \
    images/enea-edge-common.inc \
    "
REQUIRE_FILES_append_df-efi-secure-boot = " \
    classes/override_live-vm-common.inc \
    images/secure-boot.inc \
    "

require ${REQUIRE_FILES}

IMAGE_INSTALL += " \
    packagegroup-enea-virtualization-host \
    packagegroup-enea-virtualization-4gusb-modems \
    "

# Set labels for GRUB and SYSLINUX
LABELS_LIVE = "installer live-boot"

# Append default parameters for x86-64 targets
APPEND_x86-64 = "quiet console=tty0 console=ttyS0,115200"
SYSLINUX_DEFAULT_CONSOLE_x86-64 = "console=ttyS0,115200"

# Skip menu and boot installer immediately
GRUB_TIMEOUT_x86-64 = "0"
AUTO_SYSLINUXMENU_x86-64 = "0"

# If building with sota enabled, build the otaimg before the hddimg, because
# the hddimg needs it as a base image
python __anonymous() {
    ## ENEA_start ##
    if bb.utils.contains('DISTRO_FEATURES', 'sota', True, False, d):
        d.appendVarFlag("do_bootimg", "depends", " %s:do_image_ota_ext4" % d.getVar("IMAGE_BASENAME", True))
    ## ENEA_end ##
}

# Append OSTree specific parameters to the kernel command line before creating the live image
python do_bootimg_prepend () {
    ## ENEA_start ##
    if bb.utils.contains('DISTRO_FEATURES', 'sota', True, False, d):
        ostree_osname = d.getVar('OSTREE_OSNAME')
        checksum = bb.utils.sha256_file(d.getVar('DEPLOY_DIR_IMAGE') + "/" + d.getVar('OSTREE_KERNEL'))

        # The boot tree is identified through a sha256 checksum over the kernel binary
        ostree_params = " ostree=/ostree/boot.1/" + ostree_osname + "/" + checksum + "/0"

        d.setVar("APPEND", d.getVar("APPEND") + ostree_params)
    ## ENEA_end ##
}

# Before building the OSTree image, move DPKG data to /usr/dpkg, because
# OSTree does not preserve the contents of /var
IMAGE_CMD_ostree_prepend () {
    ## ENEA_start ##
    # Note: We do configure DPKG admindir at build time, but we can't do the same for APT since
    # yocto hardcodes /var/lib/dpkg paths in various places in OE-core, which we can't override,
    # so instead we move the contents to their expected location here and configure apt at runtime
    # via a configuration fragment to use the new location.
    mkdir -p usr/dpkg/lib/dpkg
    if [ "$(ls -A var/lib/dpkg)" ]; then
        mv var/lib/dpkg/* usr/dpkg/lib/dpkg/
    fi
    # In Enea Edge, the EFI binaries (shim, seloader etc.) are copied from the yocto deploy dir to
    # the installation image (hddimg) - see "classes/override_live-vm-common.inc" in this repo;
    # and during Enea Edge installation (via utils.sh) from hddimg to the destination disk.
    # Files installed by the package manager end up under OSTree /boot hardlink tree, but are
    # never actually used. To avoid confusion and save some resources, clean them up.
    rm -rf boot/*
    ## ENEA_end ##
}

IMAGE_CMD_ota_prepend () {
	## ENEA_start ##
	if [ "${OSTREE_BOOTLOADER}" = "grub" ]; then
		# Upstream bbclass creates /boot/grub2, complement it with /boot/grub
		# Note: /boot/grub2 is a dir with /boot/grub2/grub.cfg -> ../loader/grub.cfg symlink
		# while /boot/grub -> loader is a symlink to also account for /boot/grub/grubenv
		mkdir -p ${OTA_SYSROOT}/boot
		ln -s loader ${OTA_SYSROOT}/boot/grub
	fi
	## ENEA_end ##
}
