DESCRIPTION = "VNF image of the Enea Edge Runtime, includes kernel, rootfs and boot parameters"

require images/enea-edge-common.inc

IMAGE_FSTYPES += "wic.qcow2"
WKS_FILE = "enea-edge-vnf-qemux86-64.wks"

CLOUDINITPKGS = "cloud-init util-linux-blkid"

IMAGE_INSTALL += " \
    packagegroup-enea-virtualization-guest \
    nfv-init \
    iperf3 \
    ${CLOUDINITPKGS} \
    "
