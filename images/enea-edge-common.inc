require images/enea-image-common.inc

IMAGE_FEATURES += "ssh-server-openssh"

IMAGE_INSTALL += " \
    packagegroup-enea-virtualization \
    kernel-modules \
    "

# run-postinsts duplicates dpkg-configure if package_deb is used
PACKAGE_INSTALL_remove = "${@bb.utils.contains('PACKAGE_CLASSES', 'package_deb', 'run-postinsts', '', d)}"

PACKAGE_EXCLUDE_x86-64_sota += "grub-common-extras"

# After installing packages inside the rootfs, remove backup DPKG status file
rootfs_postinstall_dpkg_cleanup () {
    if ${@bb.utils.contains('PACKAGE_CLASSES', 'package_deb', 'true', 'false', d)}; then
        rm -f ${IMAGE_ROOTFS}/var/lib/dpkg/status-old
    fi
}
ROOTFS_POSTINSTALL_COMMAND_append += " rootfs_postinstall_dpkg_cleanup;"

IMAGE_FSTYPES += "ext4 ext4.gz tar.gz"

# Remove aktualizr packages from the image, we don't use it and creates odd dependencies, e.g.
# it creates a systemd unit that depends on network-online.target, which leads to enabling
# NetworkManager-wait-online.service, delaying boot up.
SOTA_CLIENT_PROV = ""
IMAGE_INSTALL_remove_sota = " \
    aktualizr \
    aktualizr-info \
    "

# We currently don't use automatic push/sign/check to a backend server, so skip running useless
# OSTree tasks during image build.
IMAGE_FSTYPES_remove_sota = " \
    ostreepush \
    garagesign \
    garagecheck \
    "
