DESCRIPTION = "Image for building the ESDK for the Host side"

require images/enea-image-common.inc

IMAGE_FEATURES += "ssh-server-openssh"

IMAGE_FEATURES += "dbg-pkgs dev-pkgs"
