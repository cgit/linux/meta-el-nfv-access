PACKAGE_INSTALL += " nfv-installer kernel-modules"

INITRAMFS_SCRIPTS = " \
    initramfs-framework-base \
    initramfs-module-udev \
    initramfs-module-setup-live \
"

# run-postinsts does not belong in the minimal initramfs
PACKAGE_INSTALL_remove = "run-postinsts"

PACKAGE_EXCLUDE_x86-64_sota += "grub-common-extras"

create_enea_symlink() {
    # enea image used in bare metal installation
    cd ${DEPLOY_DIR_IMAGE}
    ln -sf ${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.cpio.gz enea-image-minimal-initramfs-${MACHINE}.cpio.gz
    cd -
}

IMAGE_POSTPROCESS_COMMAND_append += " create_enea_symlink;"
